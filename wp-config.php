<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'key');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0)tQ|2,R;|sxY Z%K24Oh5a>99n4Y|XXV$5jD7eS,{lCW.nUQ+]9tOEj9ak(QR<+');
define('SECURE_AUTH_KEY',  'mgdl0Ik8gt@9CQIjgY|YuIjsk:9n6nHLXIoPO.-8xC#QLl}G{5jS;@HFD:A G1-F');
define('LOGGED_IN_KEY',    '?zzCsD]o82[XW!|G-M4 9a4~=0g^]UJK)#cp0ijP(%BQLPuF)u_t+~3}RS>km}{[');
define('NONCE_KEY',        'MStHK^v5,AhKa3y.X)j#:@yJOkJ.:8~uZ{8LU|T|>}El>:5H3?4lIDY9ilV`YgJ?');
define('AUTH_SALT',        'sgv#o~dKchVb`#4)b_JUFo-:Bst;D+tXgs+!z/zRv]Iy]/@=;HK!Imqbi.o]=+h$');
define('SECURE_AUTH_SALT', 'v9e%H/]Dubu)s5z77{:it@jXoH|1ASfJ (0!&-#iK!D0yKjQEyJ!GcG+23>YD&Eo');
define('LOGGED_IN_SALT',   'h#0/u((a}PJGU 0BGt:l$6MN@z$+x/@SI)$W>fd,u_J-=R~pJzIzo7#Y[Zpr+^sW');
define('NONCE_SALT',       '87I,Ou;b-;t_ZmP/KR{P71Cgb,,o|(uI=IGaQYC,-Z:`n]CYkWqXM7YzSN3lYkbf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
